import { shallowMount } from '@vue/test-utils'

import WeatherReport from '@/components/weather-report/weather-report.vue'

describe('weather-report.vue', () => {
  let wrapper: any;

  beforeEach(() => {
    wrapper = shallowMount(WeatherReport, {})
  });

  it('renders', () => {
    // Assert
    expect(wrapper.exists()).toBe(true);
  });
})

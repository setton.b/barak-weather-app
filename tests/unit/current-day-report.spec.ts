import { shallowMount } from '@vue/test-utils'

import CurrentDayReport from '@/components/weather-report/presentationals/current-day-report/current-day-report.vue'
import { CurrentDay } from '@/components/weather-report/interfaces/weather-report.interface';

describe('current-day-report.vue', () => {

  let currentProp: CurrentDay;
  let wrapper: any;

  beforeEach(() => {
    currentProp = {
      highTemp: 21,
      humidity: 20,
      uvi: 51,
      wind: '15',
      iconUrl: '01d',
    };
    wrapper = shallowMount(CurrentDayReport, {
      props: { currentProp },
    })
  });

  it('renders', () => {
    // Assert
    expect(wrapper.exists()).toBe(true); 
  })
})

import { shallowMount } from '@vue/test-utils'

import WeekDayReport from '@/components/weather-report/presentationals/week-day-report/week-day-report.vue'
import { WeekDay } from '@/components/weather-report/interfaces/weather-report.interface';

describe('week-day-report.vue', () => {
  let weatherProp: WeekDay;
  let wrapper: any;

  beforeEach(() => {
    weatherProp = {
      dayName: 'Wednesday',
      highTemp: 21,
      lowTemp: 10,
      iconUrl: '01d',
    };
    wrapper = shallowMount(WeekDayReport, {
      props: { weatherProp },
    })
  });

  it('renders', () => {
    // Assert
    expect(wrapper.exists()).toBe(true);
  })
})

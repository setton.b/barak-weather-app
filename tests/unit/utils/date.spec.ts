import { getWeekDay } from '@/lib/utils/date';

describe('date', () => {
  it('should return Monday for 1', () => {
    // Act & Assert
    expect(getWeekDay(1)).toEqual('Monday');
  });

  it('should return Tuesday for 2', () => {
    // Act & Assert
    expect(getWeekDay(2)).toEqual('Tuesday');
  });

  it('should return Wednesday for 3', () => {
    // Act & Assert
    expect(getWeekDay(3)).toEqual('Wednesday');
  });

  it('should return Thursday for 4', () => {
    // Act & Assert
    expect(getWeekDay(4)).toEqual('Thursday');
  });

  it('should return Friday for 5', () => {
    // Act & Assert
    expect(getWeekDay(5)).toEqual('Friday');
  });

  it('should return Saturday for 6', () => {
    // Act & Assert
    expect(getWeekDay(6)).toEqual('Saturday');
  });

  it('should return Sunday for 7', () => {
    // Act & Assert
    expect(getWeekDay(7)).toEqual('Sunday');
  });
})

import { convertMPStoKMPH, getDirectionFromDegree } from '@/lib/utils/wind';

describe('getDirectionFromDegree', () => {
  it('should return N for degree above 0', () => {
    // Act & Assert
    expect(getDirectionFromDegree('10')).toEqual('N');
  });

  it('should return N for degree below 0', () => {
    // Act & Assert
    expect(getDirectionFromDegree('350')).toEqual('N');
  });


  it('should return NNE', () => {
    // Act & Assert
    expect(getDirectionFromDegree('30.5')).toEqual('NNE');
  });

  it('should return NE', () => {
    // Act & Assert
    expect(getDirectionFromDegree('40.3')).toEqual('NE');
  });

  it('should return ENE', () => {
    // Act & Assert
    expect(getDirectionFromDegree('56.9')).toEqual('ENE');
  });

  it('should return E', () => {
    // Act & Assert
    expect(getDirectionFromDegree('80.7')).toEqual('E');
  });

  it('should return ESE', () => {
    // Act & Assert
    expect(getDirectionFromDegree('121')).toEqual('ESE');
  });

  it('should return SE', () => {
    // Act & Assert
    expect(getDirectionFromDegree('132.2')).toEqual('SE');
  });

  it('should return SSE', () => {
    // Act & Assert
    expect(getDirectionFromDegree('148.5')).toEqual('SSE');
  });

  it('should return S', () => {
    // Act & Assert
    expect(getDirectionFromDegree('190.3')).toEqual('S');
  });

  it('should return SSW', () => {
    // Act & Assert
    expect(getDirectionFromDegree('195.1')).toEqual('SSW');
  });


  it('should return SW', () => {
    // Act & Assert
    expect(getDirectionFromDegree('218')).toEqual('SW');
  });


  it('should return WSW', () => {
    // Act & Assert
    expect(getDirectionFromDegree('239')).toEqual('WSW');
  });

  it('should return W', () => {
    // Act & Assert
    expect(getDirectionFromDegree('259')).toEqual('W');
  });

  it('should return WNW', () => {
    // Act & Assert
    expect(getDirectionFromDegree('284.9')).toEqual('WNW');
  });

  it('should return NW', () => {
    // Act & Assert
    expect(getDirectionFromDegree('304')).toEqual('NW');
  });

  it('should return NNW', () => {
    // Act & Assert
    expect(getDirectionFromDegree('329')).toEqual('NNW');
  });
})

describe('convertMPStoKMPH', () => {
  it('should return correct conversion of meters per second to kilometers per house', () => {
    // Assemble
    const expectedKPH = '10kmh';
    const MPS = '2.778';

    // Act & Assert
    expect(convertMPStoKMPH(MPS)).toEqual(expectedKPH);
  });
})

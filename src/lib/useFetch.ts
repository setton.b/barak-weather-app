import { ref } from 'vue';

import axios from 'axios';

export const useFetch = (url: string, config: any = {}): unknown => {
  const data = ref();
  const response = ref();
  const error = ref(null);
  const isLoading = ref(false);

  const fetch = async () => {
    isLoading.value = true;
    try {
      const results = await axios.request({ url, ...config });
      response.value = results;
      data.value = results.data;
    } catch (e) {
      error.value = e;
    } finally {
      isLoading.value = false;
    }
  };

  return { response, error, data, isLoading, fetch };
}

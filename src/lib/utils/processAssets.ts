export function getImage(path: string): string {
  const images = require.context('@/assets/icons/', false);
  return images(`./${path}`);
}

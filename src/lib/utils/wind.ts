export function getDirectionFromDegree(degree: string): string {
  const degreeValue = parseFloat(degree);

  if (degreeValue >= 0 && degreeValue < 11.25 || degreeValue >= 348.75 && degreeValue <= 360) {
    return 'N';
  }

  if (degreeValue >= 11.25 && degreeValue < 33.75) {
    return 'NNE';
  }

  if (degreeValue >= 33.75 && degreeValue < 56.25) {
    return 'NE';
  }

  if (degreeValue >= 56.25 && degreeValue < 78.75) {
    return 'ENE';
  }

  if (degreeValue >= 78.75 && degreeValue < 101.25) {
    return 'E';
  }

  if (degreeValue >= 101.25 && degreeValue < 123.75) {
    return 'ESE';
  }

  if (degreeValue >= 123.75 && degreeValue < 146.25) {
    return 'SE';
  }

  if (degreeValue >= 146.25 && degreeValue < 168.75) {
    return 'SSE';
  }

  if (degreeValue >= 168.75 && degreeValue < 191.25) {
    return 'S';
  }

  if (degreeValue >= 191.25 && degreeValue < 213.75) {
    return 'SSW';
  }

  if (degreeValue >= 213.75 && degreeValue < 236.25) {
    return 'SW';
  }

  if (degreeValue >= 236.25 && degreeValue < 258.75) {
    return 'WSW';
  }

  if (degreeValue >= 258.75 && degreeValue < 281.25) {
    return 'W';
  }

  if (degreeValue >= 281.25 && degreeValue < 303.75) {
    return 'WNW';
  }

  if (degreeValue >= 303.75 && degreeValue < 326.25) {
    return 'NW';
  }

  if (degreeValue >= 326.25 && degreeValue < 348.75) {
    return 'NNW';
  }

  return 'N';
}

export function convertMPStoKMPH(meterPerSecond: string): string {
  return Math.floor(parseFloat(meterPerSecond) * 3600 / 1000) + 'kmh';
}

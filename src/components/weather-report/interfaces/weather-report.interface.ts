export interface CurrentDay {
  highTemp: number,
  humidity: number,
  uvi: number,
  wind: string,
  iconUrl: string,
}

export interface WeekDay {
  dayName: string,
  highTemp: number,
  lowTemp: number,
  iconUrl: string
}
